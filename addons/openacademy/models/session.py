# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Session(models.Model):
    _name = 'openacademy.session'
    _description = "OpenAcademy Sessions"

    name = fields.Char(required=True)
    start_date = fields.Date()
    duration = fields.Float(digits=(6, 2), help="Duration in days")
    seats = fields.Integer(string="Number of seats")


    instructor_id = fields.Many2one('res.partner', string="Instructor", domain=[('instructor','=',True)])
    course_id = fields.Many2one('openacademy.course',
        ondelete='cascade', string="Course", required=True)

    course_description = fields.Text(related='course_id.description')

    def _compute_total(self):
        for rec in self:
            harga = rec.harga
            qty = rec.qty
            total = harga * qty
            rec.total = total

    qty = fields.Integer()
    harga = fields.Integer()
    total = fields.Text(compute=_compute_total)


    attendee_ids = fields.Many2many('res.partner', string="Attendees", domain=[('is_student','=',True)])