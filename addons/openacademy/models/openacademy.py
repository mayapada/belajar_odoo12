# -*- coding: utf-8 -*-

from odoo import models, fields, api

class OpenAcademy(models.Model):
    _name = 'openacademy.openacademy'

    name = fields.Char(string="Nama", required=True)
    tgl = fields.Date(string="Tanggal")
    hand_phone = fields.Char()
    keterangan = fields.Text()
