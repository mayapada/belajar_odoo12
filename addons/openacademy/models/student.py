# -*- coding: utf-8 -*-
from odoo import fields, models

class Student(models.Model):
    _inherit = 'res.partner'

    # Add a new column to the res.partner model, by default partners are not
    # instructors
    is_student = fields.Boolean("Is Student", default=False)