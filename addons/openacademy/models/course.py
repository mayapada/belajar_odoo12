# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Course(models.Model):
    _name = 'openacademy.course'
    _description = "OpenAcademy Courses"

    name = fields.Char(string="Title", required=True)
    description = fields.Text()
    tgl_lahir = fields.Date(required=True)

    #Many2One = ambil 1
    responsible_id = fields.Many2one('res.users',
        ondelete='set null', string="Responsible", index=True)

    #One2many = biasanya untuk header - detail, detail = kepunyaan si header doang
    #contoh tranaksi penjualan
    #header - nomor , tgl, dll
    #detail - produk_id, qty, dll
    #ini berarti detail ini hanya milik header tersebut
    #misal: header = 001
    #detail: pasti hanya milik 001. gak mungkin milik 002 , 003, dll
    session_ids = fields.One2many(
        'openacademy.session', 'course_id', string="Sessions")

    #many2many
    #detail bisa jadi milik yang lain juga
    #contoh
    #kursus bahasa inggris kelas 1
    #ada instruktur = many2one <-- Pak Andi
    #ada murid-muridnya = many2many <-- [Anto], Budi, Charlie, dll

    #kursus bahasa indonesia kelas 1
    #ada instruktur = many2one <-- bu Ani
    #ada murid-muridnya = many2many <-- [Anto], Charlie, Dedi, dll

    murid = fields.Many2many('openacademy.session')
